################################################
#
# Unite scoping
#
# Ruud Kapteijn
#
# 12-Sep-18
#
################################################

library(dplyr)

pam = read.csv("PlanBee_20180910.csv")

print(paste("#customers", length(unique(pam$N_PERS))))
print(paste("#agreements", length(unique(pam$PK_CONTRACT_ID))))
print(paste("#customer-agreement links", nrow(pam)))

# number of agreements per customer
agrCus = group_by(pam, N_PERS)
agrCus = summarize(agrCus, cnt = n())
print(paste("max agr per cust", max(agrCus$cnt)))

# number of holders on an agreement
cusAgr = group_by(pam, PK_CONTRACT_ID)
cusAgr = summarize(agr, cnt = n())
print(paste("max agr holders", max(agr$cnt)))

agr = read.csv("agr.csv")
names(agr) = c("PK_CONTRACT_ID", "CLUSTER")
agr2 = group_by(agr, CLUSTER)
agr2 = summarize(agr2, cnt = n())
print(paste("max agr per cluster", max(agr2$cnt)))
for (i in 2:11) {
  print(paste(" percentage of cluster with #agreements <",i , ":", nrow(subset(agr2, cnt < i)) / nrow(agr2) * 100))
}

cus = read.csv("cus.csv")
names(cus) = c("N_PERS", "CLUSTER")
cus2 = group_by(cus, CLUSTER)
cus2 = summarize(cus2, cnt = n())
print(paste("max cus per cluster", max(cus2$cnt)))

# create random sample of 2,500 clusters and write cluster id's to sample.csv
clusters = unique(cus$CLUSTER)
set.seed(11)
sample = sample(clusters, 2500)
df.sample = data.frame(sample)
names(df.sample) = c("cluster")
# head(df.sample[order(df.sample$cluster), ])
# tail(df.sample[order(df.sample$cluster), ])
# length(unique(df.sample$cluster)) == 2500
write.csv(df.sample, "sample.csv", row.names = FALSE)
